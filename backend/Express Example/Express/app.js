const express = require("express");
const app = express();

// Routes
app.get("/", (req, res) => {
    res.send("The server is running!");
});

app.get("/helloworld", (req, res) => {
    res.send("This is hello world!");
});

app.post("/post", (req, res) => {
    res.send(`This is a post request. Request: ${req}`)
});

// Server
const port = process.env.PORT || 8081;

app.listen(port, () => {
    console.log(`The server is listening on port ${port}.`);
    console.log(`Visit http://localhost:${port}/ to verify.`)
});
