# Instructions
Make sure you have node and npm installed.

## Usage
To start, run:
```bash
$ cd Express
$ make run
```

Navigate to http://localhost:8081 to see confirmation of a successful launch.

To stop, type `Ctrl+C` in the terminal.
